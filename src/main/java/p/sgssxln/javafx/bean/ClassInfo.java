package p.sgssxln.javafx.bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 类信息
 *
 * @author wt
 * @date 2022/08/08
 */
@Data
@Accessors(chain = true)
public class ClassInfo {

    /**
     * 类名
     */
    String className;
    /**
     * 表评论
     */
    String tableRemark;
    /**
     * 包名
     */
    String packageName;


    /**
     * sql路径
     */
    String sqlPath;
    List<FiledInfo> filedList;
}
