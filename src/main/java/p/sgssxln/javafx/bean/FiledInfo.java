package p.sgssxln.javafx.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字段信息
 * @author wt
 * @date 2022/08/08
 */
@Data
@Accessors(chain = true)
public class FiledInfo {
    /**
     * 字段类型
     */
    protected String fieldType;
    /**
     * 字段名
     */
    protected String fieldName;
    /**
     * sql显示的原始名字
     */
    protected String sqlName;
    /**
     * 评论
     */
    protected String commentary;
}
