package p.sgssxln;

import cn.hutool.log.Log;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;

/**
 * 我试试
 *
 * @author wt
 * @date 2022/08/02
 */
public class Main extends Application {
    public static File basePath=new File("").getAbsoluteFile();
    protected static final Log log = Log.get();

    public static void main(String[] args) {
      launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent load = FXMLLoader.load(getClass().getResource("/fxml/index.fxml"));
        primaryStage.setScene(new Scene(load));
        primaryStage.setTitle("sql工具");
        primaryStage.show();
    }
}