open module network {
    requires hutool.all;
    requires io.vertx.core;
    requires javafx.fxml;
    requires javafx.web;
    requires javafx.media;
    requires javafx.controls;
    requires java.sql;
    requires mysql.connector.java;
    requires org.mariadb.jdbc;
    requires lombok;
    requires jsqlparser;
    requires beetl;

}