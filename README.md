# sql生成java

#### 介绍
通过sql查询结果生成javaBean和mybatis的resultMap相关代码.
做开发的时候经常出现写好sql然后再建立javaBean的情况,然后我在网上找了一圈没有想要的,就自己写了一个简单的生成器.
 **此工具是根据mybatis的columnPrefix属性做级联的,如果是直接查询表属性,那么只需要放到sql生成别名的tab页执行就好了,如果是计算的结果,则需要自己符合该属性规则** 
 **该工具生成的关联属性是1对1的,如果是1对多需要自己改改** 


#### 软件架构
软件架构说明


#### 安装教程

1.  使用jdk17
2.  ~~如果本地打包需要把beetl这个jar删除根路径的class文件,否则jdk17无法运行~~


#### 使用说明

1.  配置数据库
2.  sql添加别名
![对于需要级联的sql,生成as别名](https://foruda.gitee.com/images/1659951725902844639/屏幕截图.png "屏幕截图.png")
3.  通过sql查询生成对应的实体类和xml
![执行sql](image.png)
![生成文件目录](https://foruda.gitee.com/images/1659951919132492495/屏幕截图.png "屏幕截图.png")
![java文件内容](https://foruda.gitee.com/images/1659951983231132281/屏幕截图.png "屏幕截图.png")
![xml文件内容](https://foruda.gitee.com/images/1659952022987389325/屏幕截图.png "屏幕截图.png")
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
